async function fetchWorkouts(ordering) {
  let response = await sendRequest(
    "GET",
    `${HOST}/api/workouts/?ordering=${ordering}`
  );

  if (!response.ok) {
    throw new Error(`HTTP error! status: ${response.status}`);
  }
  let data = await response.json();
  let workouts = data.results;
  return workouts;
}
function isLastWeek(date) {
  let lastWeek = new Date();
  const today = new Date();
  const past = lastWeek.getDate() - 7;
  lastWeek.setDate(past);
  return date >= lastWeek && date <= today;
}
function isLastMonth(date) {
  let lastMonth = new Date();
  const today = new Date();
  const past = lastMonth.getDate() - 30;
  lastMonth.setDate(past);
  return date >= lastMonth && date <= today;
}
function isLastYear(date) {
  let lastYear = new Date();
  const today = new Date();
  const past = lastYear.getDate() - 365;
  lastYear.setDate(past);
  return date >= lastYear && date <= today;
}

window.addEventListener("DOMContentLoaded", async () => {
  let ordering = "-date";
  let sevenDays = 0;
  let thirtyDays = 0;
  let threeSixtyFive = 0;

  let currentUser = await getCurrentUser();
  if (ordering.includes("owner")) {
    ordering += "__username";
  }
  let workouts = await fetchWorkouts(ordering);

  for (let i = 0; i < workouts.length; i++) {
    let workout = workouts[i];

    let d = workout.date;
    let wDate = new Date(d.slice(0, 4), d.slice(5, 7) - 1, d.slice(8, 10));

    if (workout.owner == currentUser.url) {
      if (isLastWeek(wDate)) {
        sevenDays++;
      }

      if (isLastMonth(wDate)) {
        thirtyDays++;
      }
      if (isLastYear(wDate)) {
        threeSixtyFive++;
      }
    }

    document.getElementById("week").innerHTML = sevenDays;
    document.getElementById("month").innerHTML = thirtyDays;
    document.getElementById("year").innerHTML = threeSixtyFive;

  }
});
