import { Selector, ClientFunction, RequestHook } from 'testcafe';
import https from 'https';

const getLocation = ClientFunction(() => document.location.href);

fixture`login`
  .page`https://secfit-group15-frontend.herokuapp.com/login.html`;

test('login test passed', async t => { //check that you are able to log in 

  await t
    .wait(1000)
    .typeText('input[name="username"]', "admin")
    .typeText('input[name="password"]', "Password")
    .click('input[id="btn-login"]')
    .expect(getLocation()).contains('https://secfit-group15-frontend.herokuapp.com/workouts.html')
    .wait(3000);

});


test('login test fail', async t => { //check that wrong credentials are not allowed

  await t
    .wait(1000)
    .typeText('input[name="username"]', "admin")
    .typeText('input[name="password"]', "password")
    .click('input[id="btn-login"]')
    .expect(getLocation()).notContains('https://secfit-group15-frontend.herokuapp.com/workouts.html')
    .wait(1000);

});


fixture`statistics`
  .page`https://secfit-group15-frontend.herokuapp.com/login.html`

test('statistics logic', async t => { //check that the logic of the statistics is valid
  await t
    .wait(1000)
    .typeText('input[name="username"]', "admin")
    .typeText('input[name="password"]', "Password")
    .click('input[id="btn-login"]')
    .navigateTo(`https://secfit-group15-frontend.herokuapp.com/statistics.html`)
    .wait(2000)

  const week = await Selector('#week').innerText;
  const month = await Selector('#month').innerText;
  const year = await Selector('#year').innerText

  await t
    .wait(1000)
    .expect(parseInt(month)).gte(parseInt(week))
    .expect(parseInt(year)).gte(parseInt(month))
});

fixture`exercises`
  .page`https://secfit-group15-frontend.herokuapp.com/login.html`;

test('exercise value', async t => { //check that the checkboxes get checked/unchecked as intended + sort
  await t
    .wait(1000)
    .typeText('input[name="username"]', "admin")
    .typeText('input[name="password"]', "Password")
    .click('input[id="btn-login"]')
    .wait(1000)
    .navigateTo(`https://secfit-group15-frontend.herokuapp.com/workouts.html`)
    .wait(1000)
    .navigateTo(`https://secfit-group15-frontend.herokuapp.com/exercises.html`)
    .wait(1000)
    .click('input[value="Strength"]')
    .wait(1000)
    .expect(Selector('input[value="Strength"]').checked).eql(false)
    .click('input[value="Strength"]')
    .expect(Selector('input[value="Strength"]').checked).eql(true)
    .click('input[value="Endurance"]')
    .click('input[value="Strength"]')
    .click('input[value="Balance"]')
    .click('input[value="Flexibility"]')
    .click('input[value="Other"]')
    .wait(2000)
  if (await Selector('h5[class="mb-1"]').exists) {
    await t
      .click('h5[class="mb-1"]')
  }
  await t
    .wait(5000)
    .expect(getLocation()).notContains('https://secfit-group15-frontend.herokuapp.com/exercise.html') //check that no exercise exists 
    .wait(3000)
})

test('Create exercise', async t => { //check that you access "create exercise"
  await t
    .wait(1000)
    .typeText('input[name="username"]', "admin")
    .typeText('input[name="password"]', "Password")
    .click('input[id="btn-login"]')
    .wait(1000)
    .navigateTo(`https://secfit-group15-frontend.herokuapp.com/exercises.html`)
    .wait(1000)
    .click('input[id="btn-create-exercise"]')
    .wait(2000)
    .expect(getLocation()).contains('https://secfit-group15-frontend.herokuapp.com/exercise.html')
})

