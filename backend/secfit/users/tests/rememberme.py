import json

from django.test import TestCase, RequestFactory, Client
from rest_framework.test import APIRequestFactory, APIClient
from users.serializers import UserSerializer
from users.models import User
from workouts.views import RememberMe

url = '/api/remember_me/'

class RemembermeTestCase(TestCase):
    """Django test for the rememberme functionality

    tests:
        set_up:                 Set up everything needed for the test
        test_rememberme: Tests creation of rememberme for authenticated user
    """

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create(username="user",password="test")
        self.client.force_authenticate(user=self.user)

    def test_rememberme(self):
        data = {"username": "user", "password": "test"}
        response = self.client.get(url, data)
        self.assertEqual(response.status_code, 200)
        