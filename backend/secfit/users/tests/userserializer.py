import json

from django.test import TestCase, RequestFactory, Client
from users.serializers import UserSerializer
from users.models import User
from django.core.exceptions import ValidationError

url = "/api/users/"

class UserSerializerTestCase(TestCase):
    """Django test for the UserSerializer class.

    tests:
        set_up:                             Set up everything needed for the test
        test_validate_password_valid:       Test the validate password method with valid input
        test_validate_password_invalid:     Test the validate password mothod with invalid input
        test_create:                        Test creating a new user with the create method
    """

    def set_up(self):
        self.user1 = User.objects.create(username="user1")
        self.user2 = User.objects.create(username="user2", coach=self.user1)
        self.factory = RequestFactory()

    def test_validate_password_valid(self):
        password = "some_very_strong_pwd"
        serializer = UserSerializer(data = {'password': password,'password1': password})

        validated_password = serializer.validate_password(password) 
        self.assertEqual(validated_password, password)

    def test_validate_password_invalid(self):
        password = "some_very_strong_pwd"
        password1 = "Some_wrong_pwd"
        serializer = UserSerializer(data = {'password': password,'password1': password})

        serializer.validate_password(password1) 
        self.assertRaises(ValidationError)
        
    def test_create(self):
        data = {"username": "Test", "email": "test@test.no", 
                "password": "strong_pwd", "password1": "strong_pwd", 
                "phone_number": "12345678", "country": "Norway",
                "city": "Trondheim", "street_address": "Trondheimsvegen 1"}

        serializer = UserSerializer(data)
        user = serializer.create(data) 

        self.assertEqual(user.username, "Test")
        self.assertEqual(user.email, "test@test.no")
        self.assertEqual(user.phone_number, "12345678")
        self.assertEqual(user.country, "Norway")
        self.assertEqual(user.city, "Trondheim")
        self.assertEqual(user.street_address, "Trondheimsvegen 1")