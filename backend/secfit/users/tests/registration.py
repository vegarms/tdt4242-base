import json

from django.test import TestCase, RequestFactory, Client
from users.models import User

url = "/api/users/"

class RegistrationTestCase(TestCase):
    """Django test for regestering a new user.

    tests:
        set_up:                 Set up everything needed for the test
        test_registration:      Register new user
    """

    def setUp(self):
        self.client = Client()

    def test_registration(self):
        data = {"username": "Test", "email": "test@t.no", 
                "password": "strong_pwd", "password1": "strong_pwd", 
                "phone_number": "12345678", "country": "Norway",
                "city": "Trondheim", "street_address": "Trondheimsvegen 123"}
            
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 201)


class BoundaryValuesOfRegistrationTestCase(TestCase):
    """Django test for boundary values of regestering a new user.

    tests:
        set_up:                 Set up everything needed for the test
        test_minimum_data:      Register new user with as little data as possible
        test_blank_password:    Register new user without password
        test_blank_username:    Register new user without username
        test_invalid_email:     Register new user without valid email
    """

    def set_up(self):
        self.client = Client()

    def test_minimum_data(self):
        data = {"username": "t", "email": "", 
                "password": "1", "password1": "1", 
                "phone_number": "", "country": "",
                "city": "", "street_address": ""}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 201)

    def test_blank_password(self):
        data = {"username": "test", "email": "", 
                "password": "", "password1": "", 
                "phone_number": "", "country": "",
                "city": "", "street_address": ""}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 400)

    def test_blank_username(self):
        data = {"username": "", "email": "", 
                "password": "1", "password1": "1", 
                "phone_number": "", "country": "",
                "city": "", "street_address": ""}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 400)

    def test_invalid_email(self):
        data = {"username": "Test", "email": "test.no", 
                "password": "1", "password1": "1", 
                "phone_number": "", "country": "",
                "city": "", "street_address": ""}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 400)