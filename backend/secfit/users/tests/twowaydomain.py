import json

from django.test import TestCase, RequestFactory, Client
from users.serializers import UserSerializer
from users.models import User

url = "/api/users/"

test_cases = [
            {'email':'wrong',	'username':'wrong',  'password':'normal', 'password1':'normal', 'phone_number':'empty',  'country':'empty',	    'city':'empty',	    'street_address':'normal'},
            {'email':'wrong',	'username':'normal', 'password':'empty',  'password1':'normal', 'phone_number':'normal', 'country':'normal',	'city':'normal',	'street_address':'empty'},
            {'email':'wrong',	'username':'empty',  'password':'normal', 'password1':'empty',  'phone_number':'empty',  'country':'empty',	    'city':'normal',	'street_address':'normal'},
            {'email':'wrong',	'username':'normal', 'password':'empty',  'password1':'empty',  'phone_number':'empty',  'country':'normal',	'city':'normal',	'street_address':'normal'},
            {'email':'wrong',	'username':'normal', 'password':'normal', 'password1':'normal', 'phone_number':'normal', 'country':'empty',	    'city':'empty',	    'street_address':'empty'},
            {'email':'normal','username':'wrong',  'password':'empty',  'password1':'normal', 'phone_number':'normal', 'country':'empty',	    'city':'normal',	'street_address':'normal'},
            {'email':'normal','username':'normal', 'password':'normal', 'password1':'empty',  'phone_number':'empty',  'country':'normal',	'city':'normal',	'street_address':'empty'},
            {'email':'normal','username':'empty',  'password':'normal', 'password1':'normal', 'phone_number':'empty',  'country':'normal',	'city':'normal',	'street_address':'empty'},
            {'email':'normal','username':'wrong',  'password':'empty',  'password1':'empty',  'phone_number':'normal', 'country':'normal',	'city':'empty',	    'street_address':'normal'},
            {'email':'empty',	'username':'empty',  'password':'empty',  'password1':'empty',  'phone_number':'normal', 'country':'normal',	'city':'empty',	    'street_address':'normal'},
            {'email':'empty',	'username':'wrong',  'password':'normal', 'password1':'normal', 'phone_number':'empty',  'country':'normal',	'city':'normal',	'street_address':'empty'},
            {'email':'empty',	'username':'normal', 'password':'empty',  'password1':'normal', 'phone_number':'normal', 'country':'empty',	    'city':'empty',	    'street_address':'normal'},
            {'email':'empty',	'username':'normal', 'password':'normal', 'password1':'empty',  'phone_number':'empty',  'country':'normal',	'city':'normal',	'street_address':'empty'},
            {'email':'empty',	'username':'empty',  'password':'empty',  'password1':'normal', 'phone_number':'normal', 'country':'empty',	    'city':'normal',	'street_address':'normal'},
            {'email':'empty',	'username':'wrong',  'password':'normal', 'password1':'empty',  'phone_number':'normal', 'country':'normal',	'city':'empty',	    'street_address':'empty'},
            {'email':'empty',	'username':'normal', 'password':'normal', 'password1':'normal', 'phone_number':'empty',  'country':'empty',	    'city':'normal',	'street_address':'normal'},
            {'email':'wrong',	'username':'wrong',  'password':'empty',  'password1':'empty',  'phone_number':'normal', 'country':'normal',	'city':'normal',	'street_address':'normal'},
            {'email':'wrong',	'username':'empty',  'password':'normal', 'password1':'normal', 'phone_number':'normal', 'country':'normal',	'city':'empty',	    'street_address':'empty'},
            {'email':'wrong',	'username':'wrong',  'password':'normal', 'password1':'empty',  'phone_number':'empty',  'country':'empty',	    'city':'normal',	'street_address':'normal'},
            {'email':'wrong',	'username':'empty',  'password':'normal', 'password1':'normal', 'phone_number':'empty',  'country':'empty',	    'city':'empty',	    'street_address':'normal'},
            {'email':'normal','username':'normal', 'password':'empty',  'password1':'normal', 'phone_number':'empty',  'country':'normal',	'city':'empty',	    'street_address':'normal'},
            {'email':'normal','username':'normal', 'password':'normal', 'password1':'empty',  'phone_number':'normal', 'country':'empty',	    'city':'normal',	'street_address':'empty'},
            {'email':'normal','username':'empty',  'password':'normal', 'password1':'normal', 'phone_number':'empty',  'country':'normal',	'city':'empty',	    'street_address':'normal'},
            {'email':'normal','username':'wrong',  'password':'empty',  'password1':'normal', 'phone_number':'normal', 'country':'empty',	    'city':'normal',	'street_address':'empty'},
            {'email':'normal','username':'normal', 'password':'normal', 'password1':'empty',  'phone_number':'normal', 'country':'normal',	'city':'empty',	    'street_address':'normal'},
            {'email':'normal','username':'empty',  'password':'empty',  'password1':'normal', 'phone_number':'empty',  'country':'normal',	'city':'normal',	'street_address':'empty'},
            {'email':'normal','username':'wrong',  'password':'normal', 'password1':'empty',  'phone_number':'normal', 'country':'empty',	    'city':'normal',	'street_address':'normal'}
        ]

case_data = {
            'normal': {
                'email': 'test@test.no',
                'username': 'test',
                'password': 'test123',
                'password1': 'test123',
                'phone_number': 12354678,
                'country': 'Norway',
                'city': 'Trondheim',
                'street_address': 'Trondheimsvegen',
            },
            'empty': {
                'email': '',
                'username': '',
                'password': '',
                'password1': '',
                'phone_number': '',
                'country': '',
                'city': '',
                'street_address': '',
            },
            'wrong': {
                'email': 'test.no',
                'username': 'test]',
            }
        }

class TwoWayDomainTestOfRegistrationTestCase(TestCase):
    """Django two-way domain test for registration of new user.

    tests:
        set_up:                             Set up everything needed for the test and define test cases
        test_domain_registration            Tests all test cases and evalutes them against expected responses
    """

    def set_up(self):
        self.client = Client()

    def test_domain_registration(self):
        for case in test_cases:
            data = {}
            
            for key, value in case.items():
                data[key] = case_data[value][key]
            code = 201
            
            if case['email'] == 'wrong' or case['username'] == 'wrong' or case['password'] == 'empty' or case['username'] == 'empty' or case['password1'] == 'empty':
                code = 400

            response = self.client.post(url, data)
            self.assertEqual(response.status_code, code)
            