import json

from django.test import TestCase, RequestFactory, Client
from workouts.models import Workout, Exercise, ExerciseInstance, WorkoutFile
from users.models import User
from comments.models import Comment

url_workouts = '/api/workouts/'
url_workout_files = '/api/workout-files/'
url_comments = '/api/comments/'
url_token = '/api/token/'
url_path = '/path/'
bearer = 'Bearer '
date = "2021-03-06T18:00:00Z"

class AthleteAccessTestCase(TestCase):
    """Django test for FR5 - visibility of details, files and comments for Athletes

    tests:
        set_up:                                 Set up everything needed for the test
        test_visibility_on_private_content:     Test vilibility on private content
        test_visibility_on_public_content:      Test visibility on public content
    """

    def setUp(self):
        self.client = Client()

        # Create user
        self.athlete = User.objects.create(username="user")
        self.athlete.set_password('user')
        self.athlete.save()

        # Authenticate athlete
        response = self.client.post(url_token, {'username': 'user', 'password': 'user'})
        content = json.loads(response.content)
        self.client.defaults['HTTP_AUTHORIZATION'] = bearer + content['access']

    def test_visibility_on_private_content(self):
        workout = Workout.objects.create(name="workout", date=date, notes="notes", owner=self.athlete, visibility="PR")
        comment = Comment.objects.create(content="comment", timestamp=date, owner=self.athlete, workout=workout)
        file = WorkoutFile.objects.create(file=url_path, owner=self.athlete, workout=workout)

        response = self.client.get(url_workouts+str(workout.id)+'/')
        self.assertEqual(response.status_code, 200)

        response = self.client.get(url_comments+str(comment.id)+'/')
        self.assertEqual(response.status_code, 200)

        response = self.client.get(url_workout_files+str(file.id)+'/')
        self.assertEqual(response.status_code, 200)

    def test_visibility_on_public_content(self):
        workout = Workout.objects.create(name="workout", date=date, notes="notes", owner=self.athlete, visibility="PU")
        comment = Comment.objects.create(content="comment", timestamp=date, owner=self.athlete, workout=workout)
        file = WorkoutFile.objects.create(file=url_path, owner=self.athlete, workout=workout)

        response = self.client.get(url_workouts+str(workout.id)+'/')
        self.assertEqual(response.status_code, 200)

        response = self.client.get(url_comments+str(comment.id)+'/')
        self.assertEqual(response.status_code, 200)

        response = self.client.get(url_workout_files+str(file.id)+'/')
        self.assertEqual(response.status_code, 200)

class CoachAccessTestCase(TestCase):
    """Django test for FR5 - visibility of details, files and comments for Coaches

    tests:
        set_up:                                 Set up everything needed for the test
        test_visibility_on_private_content:     Test vilibility on private content
        test_visibility_on_coach_content:       Test visibility on coach content
    """

    def setUp(self):
        self.client = Client()

        # Create coach
        self.coach = User.objects.create(username="user1")
        self.coach.set_password('user1')
        self.coach.save()

        # Create user
        self.athlete = User.objects.create(username="user2", coach = self.coach)
        self.athlete.set_password('user2')
        self.athlete.save()

        # Authenticate coach
        response = self.client.post(url_token, {'username': 'user1', 'password': 'user1'})
        content = json.loads(response.content)
        self.client.defaults['HTTP_AUTHORIZATION'] = bearer + content['access']

    def test_visibility_on_private_content(self):
        workout = Workout.objects.create(name="workout", date=date, notes="notes", owner=self.athlete, visibility="PR")
        comment = Comment.objects.create(content="comment", timestamp=date, owner=self.athlete, workout=workout)
        file = WorkoutFile.objects.create(file=url_path, owner=self.athlete, workout=workout)

        response = self.client.get(url_workouts+str(workout.id)+'/')
        self.assertEqual(response.status_code, 200)

        response = self.client.get(url_comments+str(comment.id)+'/')
        self.assertEqual(response.status_code, 403)

        response = self.client.get(url_workout_files+str(file.id)+'/')
        self.assertEqual(response.status_code, 200)

    def test_visibility_on_coach_content(self):
        workout = Workout.objects.create(name="workout", date=date, notes="notes", owner=self.athlete, visibility="CO")
        comment = Comment.objects.create(content="comment", timestamp=date, owner=self.athlete, workout=workout)
        file = WorkoutFile.objects.create(file=url_path, owner=self.athlete, workout=workout)

        response = self.client.get(url_workouts+str(workout.id)+'/')
        self.assertEqual(response.status_code, 200)

        response = self.client.get(url_comments+str(comment.id)+'/')
        self.assertEqual(response.status_code, 200)

        response = self.client.get(url_workout_files+str(file.id)+'/')
        self.assertEqual(response.status_code, 200)

class UserAccessTestCase(TestCase):
    """Django test for FR5 - visibility of details, files and comments for other users

    tests:
        set_up:                                 Set up everything needed for the test
        test_visibility_on_private_content:     Test vilibility on private content
        test_visibility_on_coach_content:       Test visibility on coach content
    """

    def setUp(self):
        self.client = Client()

        # Create user
        self.athlete = User.objects.create(username="user1")
        self.athlete.set_password('user1')
        self.athlete.save()

        self.other = User.objects.create(username="user2")
        self.other.set_password('user2')
        self.other.save()

        # Authenticate user
        response = self.client.post(url_token, {'username': 'user2', 'password': 'user2'})
        content = json.loads(response.content)
        self.client.defaults['HTTP_AUTHORIZATION'] = bearer + content['access']

    def test_visibility_on_private_content(self):
        workout = Workout.objects.create(name="workout", date=date, notes="notes", owner=self.athlete, visibility="PR")
        comment = Comment.objects.create(content="comment", timestamp=date, owner=self.athlete, workout=workout)
        file = WorkoutFile.objects.create(file=url_path, owner=self.athlete, workout=workout)

        response = self.client.get(url_workouts+str(workout.id)+'/')
        self.assertEqual(response.status_code, 403)

        response = self.client.get(url_comments+str(comment.id)+'/')
        self.assertEqual(response.status_code, 403)

        response = self.client.get(url_workout_files+str(file.id)+'/')
        self.assertEqual(response.status_code, 403)

    def test_visibility_on_coach_content(self):
        workout = Workout.objects.create(name="workout", date=date, notes="notes", owner=self.athlete, visibility="CO")
        comment = Comment.objects.create(content="comment", timestamp=date, owner=self.athlete, workout=workout)
        file = WorkoutFile.objects.create(file=url_path, owner=self.athlete, workout=workout)

        response = self.client.get(url_workouts+str(workout.id)+'/')
        self.assertEqual(response.status_code, 403)

        response = self.client.get(url_comments+str(comment.id)+'/')
        self.assertEqual(response.status_code, 403)

        response = self.client.get(url_workout_files+str(file.id)+'/')
        self.assertEqual(response.status_code, 403)
