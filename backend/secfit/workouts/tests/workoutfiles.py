import json

from django.test import TestCase, RequestFactory, Client
from rest_framework.test import APIRequestFactory, APIClient
from workouts.models import Workout, Exercise, ExerciseInstance, WorkoutFile
from workouts.serializers import WorkoutSerializer
from users.models import User

class WorkoutFilesTestCase(TestCase):
    """Django test for perserving the functionality of workout-files

    tests:
        set_up:                 Set up everything needed for the test
    """
    def setUp(self):
        self.user = User.objects.create(username="user")
        self.workout = Workout.objects.create(name="workout", date="2021-03-06T18:00:00Z", notes="note", 
                                              owner=self.user, visibility="PU")
        self.exercise = Exercise.objects.create(name="exercise", description="test", unit="test")
        
    def test_workout_files(self):
        data = {'name': 'workout', 'notes': 'note', 'visibility': 'PU',
                    'date': '2021-03-06T18:00:00.000Z', 
                    'exercise_instances': [{'exercise': self.exercise, 'sets':12, 'number':12}],
                    'files': [{'owner': self.user, 'file':'workouts/1/document.pdf'}]
        }
        WorkoutSerializer(data).update(self.workout, data)

        instance = ExerciseInstance.objects.filter(workout=self.workout).first()
        self.assertEqual(instance.exercise, self.exercise)

        files = WorkoutFile.objects.filter(workout=self.workout).first()
        self.assertEqual(files.owner, self.user)
        self.assertEqual(files.file, 'workouts/1/document.pdf')
