from workouts.tests.permissions import *
from workouts.tests.workouts import *
from workouts.tests.visibility import *
from workouts.tests.features import *
from workouts.tests.workoutfiles import *
