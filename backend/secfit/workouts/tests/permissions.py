import json

from django.test import TestCase, RequestFactory, Client
from workouts.permissions import (
    IsOwner, 
    IsOwnerOfWorkout, 
    IsCoachAndVisibleToCoach, 
    IsCoachOfWorkoutAndVisibleToCoach, 
    IsPublic,
    IsWorkoutPublic,
    IsReadOnly
)
from workouts.models import Workout, Exercise, ExerciseInstance, WorkoutFile
from users.models import User

url_workout1 = '/api/workouts/1/'
date = "2021-03-02T18:00:00Z"

class IsOwnerTestCase(TestCase):
    """Django test for the permission-class IsOwner in workout/permissions

    tests:
        set_up:                                 Set up everything needed for the test
        test_is_owner_valid:                    Test a input where user is owner
        test_is_owner_invalid:                  Test a input where user is not owner
    """

    def setUp(self):
        self.user1 = User.objects.create(username="user1")
        self.user2 = User.objects.create(username="user2")
        self.factory = RequestFactory()
        self.isOwner = IsOwner()
        self.workout = Workout.objects.create(name="test", date=date, notes="note", owner=self.user1, visibility="PU")

    def test_is_owner_valid(self):
        request = self.factory.get('/')
        request.user = self.user1
        has_permission = self.isOwner.has_object_permission(request, None, self.workout)
        self.assertTrue(has_permission)

    def test_is_owner_invalid(self):
        request = self.factory.get('/')
        request.user = self.user2
        has_permission = self.isOwner.has_object_permission(request, None, self.workout)
        self.assertFalse(has_permission)

class IsOwnerOfWorkoutTestCase(TestCase):
    """Django test for the permission-class IsOwnerOfWorkout in workout/permissions

    tests:
        set_up:                                 Set up everything needed for the test
        test_has_permission_valid:              Test a input where user should have permission
        test_has_permission_invalid:            Input where user should not have permission
        test_has_object_permission_valid:       Test a input where user should have permission
        test_has_object_permission_invalid:     Input where user should not have permission
    """

    def setUp(self):
        self.user1 = User.objects.create(username="user1")
        self.user2 = User.objects.create(username="user2")
        self.factory = RequestFactory()
        self.isOwnerOfWorkout = IsOwnerOfWorkout()
        self.workout = Workout.objects.create(name="test", date=date, notes="note", owner=self.user1, visibility="PU")

    def test_has_permission_valid(self):
        request = self.factory.get('/')
        request.user = self.user1
        request.data = { 'workout' : url_workout1}
        has_permission = self.isOwnerOfWorkout.has_permission(request, None)
        self.assertTrue(has_permission)

        request = self.factory.post('/')
        request.user = self.user1
        request.data = { 'workout' : url_workout1}
        has_permission = self.isOwnerOfWorkout.has_permission(request, None)
        self.assertTrue(has_permission)

    def test_has_permission_invalid(self):
        request = self.factory.post('/')
        request.user = self.user2
        request.data = { 'workout' : url_workout1}
        has_permission = self.isOwnerOfWorkout.has_permission(request, None)
        self.assertFalse(has_permission)
        
        request = self.factory.post('/')
        request.user = self.user1
        request.data = { 'workout' : None}
        has_permission = self.isOwnerOfWorkout.has_permission(request, None)
        self.assertFalse(has_permission)

    def test_has_object_permission_valid(self):
        request = self.factory.get('/')
        request.user = self.user1
        exercise = Exercise.objects.create(name="Test", description="Test", unit="Test")
        instance = ExerciseInstance.objects.create(workout=self.workout, exercise=exercise, sets=1, number=1)

        has_permission = self.isOwnerOfWorkout.has_object_permission(request, None, instance)
        self.assertTrue(has_permission)

    def test_has_object_permission_invalid(self):
        request = self.factory.get('/')
        request.user = self.user2
        exercise = Exercise.objects.create(name="Test", description="Test", unit="Test")
        instance = ExerciseInstance.objects.create(workout=self.workout, exercise=exercise, sets=1, number=1)
        has_permission = self.isOwnerOfWorkout.has_object_permission(request, None, instance)
        self.assertFalse(has_permission)
        
class IsCoachAndVisibleToCoachTestCase(TestCase):
    """Django test for the permission-class IsCoachAndVisibleToCoach in workout/permissions

    tests:
        set_up:                                     Set up everything needed for the test
        test_is_coach_and_visible_to_coach_valid:   Test where user is coach and checks that it is visible
        test_is_coach_and_visible_to_coach_invalid: Test where user is not coach and checks that it is not visible
    """

    def setUp(self):
        self.user3 = User.objects.create(username="user3")
        self.user2 = User.objects.create(username="user2")
        self.user1 = User.objects.create(username="user1", coach=self.user2)

        self.factory = RequestFactory()
        self.isCoachAndVisibleToCoach = IsCoachAndVisibleToCoach()
        self.workout = Workout.objects.create(name="test", date=date, notes="note", owner=self.user1, visibility="PU")

    def test_is_coach_and_visible_to_coach_valid(self):
        request = self.factory.get('/')
        request.user = self.user2
        has_permission = self.isCoachAndVisibleToCoach.has_object_permission(request, None, self.workout)
        self.assertTrue(has_permission)

    def test_is_coach_and_visible_to_coach_invalid(self):
        request = self.factory.get('/')
        request.user = self.user3
        has_permission = self.isCoachAndVisibleToCoach.has_object_permission(request, None, self.workout)
        self.assertFalse(has_permission)



class IsCoachOfWorkoutAndVisibleToCoachTestCase(TestCase):
    """Django test for the permission-class IsCoachOfWorkoutAndVisibleToCoach in workout/permissions

    tests:
        set_up:                                                 Set up everything needed for the test
        test_is_coach_of_workout_and_visible_to_coach_valid:    Names describes itself
        test_is_coach_of_workout_and_visible_to_coach_invalid:  Names describes itself
    """

    def setUp(self):
        self.user3 = User.objects.create(username="user3")
        self.user2 = User.objects.create(username="user2")
        self.user1 = User.objects.create(username="user1", coach=self.user2)

        self.factory = RequestFactory()
        self.isCoachOfWorkoutAndVisibleToCoach = IsCoachOfWorkoutAndVisibleToCoach()
        self.workout = Workout.objects.create(name="test", date=date, notes="note", owner=self.user1, visibility="PU")

    def test_is_coach_of_workout_and_visible_to_coach_valid(self):
        request = self.factory.get('/')
        request.user = self.user2
        exercise = Exercise.objects.create(name="Test", description="Test", unit="Test")
        instance = ExerciseInstance.objects.create(workout=self.workout, exercise=exercise, sets=1, number=1)
        has_permission = self.isCoachOfWorkoutAndVisibleToCoach.has_object_permission(request, None, instance)
        self.assertTrue(has_permission)

    def test_is_coach_of_workout_and_visible_to_coach_invalid(self):
        request = self.factory.get('/')
        request.user = self.user3
        exercise = Exercise.objects.create(name="Test", description="Test", unit="Test")
        instance = ExerciseInstance.objects.create(workout=self.workout, exercise=exercise, sets=1, number=1)
        has_permission = self.isCoachOfWorkoutAndVisibleToCoach.has_object_permission(request, None, instance)
        self.assertFalse(has_permission)

class IsPublicTestCase(TestCase):
    """Django test for the permission-class IsPublic in workout/permissions

    tests:
        set_up:                         Set up everything needed for the test
        test_is_public_valid:           Test if public workout is public
        test_is_public_invalid:         Test if not public workout is not public
    """

    def setUp(self):
        self.user1 = User.objects.create(username="user1")
        self.user2 = User.objects.create(username="user2")

        self.factory = RequestFactory()
        self.isPublic = IsPublic()

    def test_is_public_valid(self):
        request = self.factory.get('/')
        request.user = self.user1
        workout = Workout.objects.create(name="test", date=date, notes="note", owner=self.user1, visibility="PU")

        is_public = self.isPublic.has_object_permission(request, None, workout)
        self.assertTrue(is_public)

    def test_is_public_invalid(self):
        request = self.factory.get('/')
        request.user = self.user1
        workout = Workout.objects.create(name="test", date=date, notes="note", owner=self.user1, visibility="CO")

        is_public = self.isPublic.has_object_permission(request, None, workout)
        self.assertFalse(is_public)

class IsWorkoutPublicTestCase(TestCase):
    """Django test for the permission-class IsWorkoutPublic in workout/permissions

    tests:
        set_up:                             Set up everything needed for the test
        test_is_workout_public_valid:       Test if public workout is public
        test_is_workout_public_invalid:     Test if not public workout is not public
    """

    def setUp(self):
        self.user1 = User.objects.create(username="user1")
        self.user2 = User.objects.create(username="user2")

        self.factory = RequestFactory()
        self.isWorkoutPublic = IsWorkoutPublic()

    def test_is_workout_public_valid(self):
        request = self.factory.get('/')
        request.user = self.user1
        workout = Workout.objects.create(name="test", date=date, notes="note", owner=self.user1, visibility="PU")
        exercise = Exercise.objects.create(name="Test", description="Test", unit="Test")
        instance = ExerciseInstance.objects.create(workout=workout, exercise=exercise, sets=1, number=1)

        is_public = self.isWorkoutPublic.has_object_permission(request, None, instance)
        self.assertTrue(is_public)

    def test_is_workout_public_invalid(self):
        request = self.factory.get('/')
        request.user = self.user1
        workout = Workout.objects.create(name="test", date=date, notes="note", owner=self.user1, visibility="CO")
        exercise = Exercise.objects.create(name="Test", description="Test", unit="Test")
        instance = ExerciseInstance.objects.create(workout=workout, exercise=exercise, sets=1, number=1)

        is_public = self.isWorkoutPublic.has_object_permission(request, None, instance)
        self.assertFalse(is_public)

class IsReadOnlyTestCase(TestCase):
    """Django test for the permission-class IsWorkoutPublic in workout/permissions

    tests:
        set_up:                             Set up everything needed for the test
        test_is_read_only_valid:            Test if workout is readonly not owner user
        test_is_read_only_invalid:          Test if workout is not readonly for owner
    """

    def setUp(self):
        self.user1 = User.objects.create(username="user1")
        self.user2 = User.objects.create(username="user2")

        self.factory = RequestFactory()
        self.isReadOnly = IsReadOnly()
        self.workout = Workout.objects.create(name="test", date=date, notes="note", owner=self.user1, visibility="PU")

    def test_is_read_only_valid(self):
        request = self.factory.get('/')
        request.user = self.user1

        is_readonly = self.isReadOnly.has_object_permission(request, None, self.workout)
        self.assertTrue(is_readonly)

    def test_is_read_only_invalid(self):
        request = self.factory.post('/')
        request.user = self.user1

        is_readonly = self.isReadOnly.has_object_permission(request, None, self.workout)
        self.assertFalse(is_readonly)
