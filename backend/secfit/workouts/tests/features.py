import json

from django.test import TestCase, RequestFactory, Client
from rest_framework.test import APIRequestFactory, APIClient
from workouts.models import Workout, Exercise, ExerciseInstance, WorkoutFile
from users.models import User

class CreateWorkoutForAthleteTestCase(TestCase):
    """Django test for UC1 - create workout for athlete as coach

    tests:
        set_up:                                 Set up everything needed for the test
        test_create_workout_as_coach:           Creates a workout as coach
        test_check_coach:                       Checks that athlete is set as owner
    """

    def setUp(self):
        self.client = APIClient()
        self.coach = User.objects.create(username="coach",password="coach")
        self.athlete = User.objects.create(username="athlete",password="athlete", coach=self.coach)
        self.client.force_authenticate(user=self.coach)
        self.request = json.loads('{"name": "test", "date": "2021-03-06T18:00:00.000Z", "notes": "note", "visibility": "PU", "exercise_instances": [], "filename": []}')

    def test_create_workout_as_coach(self):
        self.request["owner"] = "athlete"
        request = self.client.post('/api/workouts/', json.dumps(self.request), content_type='application/json')
        self.assertEquals(request.status_code,201)

    def test_check_coach(self):
        workout = Workout.objects.create(name="test", date="2021-03-02T18:00:00Z", notes="note", owner=self.athlete, visibility="PU")
        self.assertEqual(self.athlete, workout.owner)

class CategorizeExerciseTestCase(TestCase):
    """Django test for UC4 - categorize exercise

    tests:
        set_up:                                 Set up everything needed for the test
        test_create_categorized_excercise:      Creates categorized exercise
        test_check_categorized_exercise:        Checks category of exercise
    """

    def setUp(self):
        self.client = Client()

        # Create user
        self.user = User.objects.create(username="user")
        self.user.set_password('user')
        self.user.save()

        # Authenticate user
        response = self.client.post('/api/token/', {'username': 'user', 'password': 'user'})
        content = json.loads(response.content)
        self.client.defaults['HTTP_AUTHORIZATION'] = 'Bearer ' + content['access']

    def test_create_categorized_exercise(self):
        data = {"name": "Test", "description": "Test", 
                "category": "Endurance", "unit": "kg"}
        
        response = self.client.post("/api/exercises/", data)
        self.assertEqual(response.status_code, 201)
    
    def test_check_categorized_exercise(self):
        exercise = Exercise.objects.create(name="test", description="test", category="Endurance", unit="kg")
        self.assertEqual("Endurance", exercise.category)
