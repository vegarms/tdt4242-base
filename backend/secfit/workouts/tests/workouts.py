import json

from django.test import TestCase, RequestFactory, Client
from rest_framework.test import APIRequestFactory, APIClient
from workouts.models import Workout, Exercise, ExerciseInstance, WorkoutFile
from users.models import User

url_workout = '/api/workouts/'
content_type = 'application/json'

class BoundaryValuesOfNewWorkout(TestCase):
    """Django test for boundary values of registering a new workout.

    tests:
        set_up:                 Set up everything needed for the test
        test_name:              Test boundary values of name field
        test_date:              Test boundary values of date field
        test_notes:             Test boundary values of notes field
        test_visibility:        Test boundary values of visibility field
    """

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create(username="user",password="user")
        self.client.force_authenticate(user=self.user)
        self.request = json.loads('{"name": "test", "date": "2021-03-06T18:00:00.000Z", "notes": "note", "visibility": "PU", "exercise_instances": [], "filename": []}')

    def test_name(self):
        self.request["name"] = ""
        request = self.client.post(url_workout, json.dumps(self.request), content_type=content_type)
        self.assertEquals(request.status_code,400)
 
        self.request["name"] = "test"
        request = self.client.post(url_workout, json.dumps(self.request), content_type=content_type)
        self.assertEquals(request.status_code,201) 

        self.request["name"] = "@€xrgrdh"
        request = self.client.post(url_workout, json.dumps(self.request), content_type=content_type)
        self.assertEquals(request.status_code,201)

        self.request["name"] = "1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111" #100
        request = self.client.post(url_workout, json.dumps(self.request), content_type=content_type)
        self.assertEquals(request.status_code,201) 

        self.request["name"] = "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111" #101
        request = self.client.post(url_workout, json.dumps(self.request), content_type=content_type)
        self.assertEquals(request.status_code,400)


    def test_date(self):
        self.request["date"] = ""
        request = self.client.post(url_workout, json.dumps(self.request), content_type=content_type)
        self.assertEquals(request.status_code,400)

        self.request["date"] = "2021-32-32T18:00:00.000Z"
        request = self.client.post(url_workout, json.dumps(self.request), content_type=content_type)
        self.assertEquals(request.status_code,400)

        self.request["date"] = "2021-03-08T18:00:00.000Z"
        request = self.client.post(url_workout, json.dumps(self.request), content_type=content_type)
        self.assertEquals(request.status_code,201) 

    def test_notes(self):
        self.request["notes"] = ""
        request = self.client.post(url_workout, json.dumps(self.request), content_type=content_type)
        self.assertEquals(request.status_code,400)

        self.request["notes"] = "test"
        request = self.client.post(url_workout, json.dumps(self.request), content_type=content_type)
        self.assertEquals(request.status_code,201)

    def test_visibility(self):
        self.request["visibility"] = ""
        request = self.client.post(url_workout, json.dumps(self.request), content_type=content_type)
        self.assertEquals(request.status_code,400)

        self.request["visibility"] = "XX"
        request = self.client.post(url_workout, json.dumps(self.request), content_type=content_type)
        self.assertEquals(request.status_code,400) 

        self.request["visibility"] = "PRI"
        request = self.client.post(url_workout, json.dumps(self.request), content_type=content_type)
        self.assertEquals(request.status_code,400)

        self.request["visibility"] = "PR"
        request = self.client.post(url_workout, json.dumps(self.request), content_type=content_type)
        self.assertEquals(request.status_code,201)
